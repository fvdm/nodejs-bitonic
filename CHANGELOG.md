#### 1.0.1 (2017-09-06)

##### Chores

* **example:** Fixed config ([61333352](https://github.com/fvdm/nodejs-bitonic/commit/6133335218dac8b1d6f6243e141f92535915aaf9))

##### Documentation Changes

* **readme:** Fixed config ([90ca5dff](https://github.com/fvdm/nodejs-bitonic/commit/90ca5dff572b2ba4e45867c4fd40ccc3c74e8f55))

## 1.0.0 (2017-09-06)

### 0.2.0 (2017-09-04)

##### Documentation Changes

* **readme:**
  * Fixed autocorrect typos ([cb451cef](https://github.com/fvdm/nodejs-bitonic/commit/cb451cef7a1dee83b0e0febaae46e7f7494b9466))
  * Added method parameter to price.buy ([31a63281](https://github.com/fvdm/nodejs-bitonic/commit/31a63281e9dfe3598853afa786b603bb0619bc90))
  * Optional params now required ([7a0bdf03](https://github.com/fvdm/nodejs-bitonic/commit/7a0bdf03150fd2f7fc54043803436e756c27a158))

##### New Features

* **priceBuy:** Added method parameter ([802d7ce0](https://github.com/fvdm/nodejs-bitonic/commit/802d7ce058860f76eff689614a893cb0cd76e4f1))

##### Refactors

* **priceBuy:** Optional params now required ([fef7948a](https://github.com/fvdm/nodejs-bitonic/commit/fef7948a7c13da17b7c54fa3efbe61385b24499b))
* **priceSell:** Optional params now required ([edb55835](https://github.com/fvdm/nodejs-bitonic/commit/edb558358ac060a825790db3739630bd32b81b70))

##### Tests

* **main:**
  * Added price.sell test ([fa754f70](https://github.com/fvdm/nodejs-bitonic/commit/fa754f7062dbf54dfc12fa11dfadc8ff75bbd668))
  * Added price.buy with method param ([3eb9d934](https://github.com/fvdm/nodejs-bitonic/commit/3eb9d93431e17306184ab48c79d28a3d2193770f))
  * Added price.buy with default method ([bf483fdf](https://github.com/fvdm/nodejs-bitonic/commit/bf483fdf44a4d124abdcf16a14802ccba25534de))

#### 0.1.1 (2017-09-01)

##### Bug Fixes

* **request:** Fixed syntax typo ([d7433034](https://github.com/fvdm/nodejs-bitonic/commit/d7433034c9daea4e76a9723aaafd2b73550b16de))

##### Other Changes

* **request:** Added user-agent header ([733abce6](https://github.com/fvdm/nodejs-bitonic/commit/733abce653ca0e30cb94c89e0c8471d02c7905d6))

##### Refactors

* **response:** API always returns JSON data ([e8be4e42](https://github.com/fvdm/nodejs-bitonic/commit/e8be4e42a9c9e61b70ce7bda340092131914a3c9))

##### Tests

* **main:** Add timeout error test ([96570758](https://github.com/fvdm/nodejs-bitonic/commit/96570758cdbf42ffed3e0ffef72c13e30f4467ec))

### 0.1.0 (2017-08-31)

##### Chores

* **example:** Fixed data refs ([43d802ba](https://github.com/fvdm/nodejs-bitonic/commit/43d802ba012c2a5900a8a1b7999240d9be237435))

##### Documentation Changes

* **badges:** Added status badges to README.md ([0b97ba78](https://github.com/fvdm/nodejs-bitonic/commit/0b97ba78747db331db23e8d63b56d2bfd7d35d95))
* **readme:**
  * Fixed typos ([5d4e974d](https://github.com/fvdm/nodejs-bitonic/commit/5d4e974db6bd86e36ecd497a25b73972c2081062))
  * Added methods with examples ([42cddb16](https://github.com/fvdm/nodejs-bitonic/commit/42cddb1686030c599aff08dec6f4b43e8beeff37))
  * Added README.md contents ([ed60075c](https://github.com/fvdm/nodejs-bitonic/commit/ed60075c790d857e8b83ca71a77be000a6e05702))

##### Bug Fixes

* **request:** Fixed invalid request path ([8d19474e](https://github.com/fvdm/nodejs-bitonic/commit/8d19474e9b434b8e4ea8607f3d2e7f11f530bf0b))
* **setup:**
  * Fixed bad variable ref ([974a1de6](https://github.com/fvdm/nodejs-bitonic/commit/974a1de6544ff3b0239e217a54476aab98a2368e))
  * Fixed syntax error ([bd9f24e5](https://github.com/fvdm/nodejs-bitonic/commit/bd9f24e5f5f9d84b43aecc27972f2f26f6126827))
  * Fixed object iteration ([dced45c5](https://github.com/fvdm/nodejs-bitonic/commit/dced45c51ffbff51413beb590b8950252bfa4733))

##### Refactors

* **package:** Minimum node v6.0 ([57dffeec](https://github.com/fvdm/nodejs-bitonic/commit/57dffeec00b9062211e499587fd3d0700e2c0bf3))

##### Code Style Changes

* **comments:** Fixed JSDoc optional params ([f2085fa6](https://github.com/fvdm/nodejs-bitonic/commit/f2085fa6783df95f5c04c9048e46b6628ce4389f))
* **lint:** Fixed lint errors ([95a3c787](https://github.com/fvdm/nodejs-bitonic/commit/95a3c787bd80fb3b299e5fe4704fad850fb37935))

##### Tests

* **travis:** Minimum node version v6.0 ([47a96a25](https://github.com/fvdm/nodejs-bitonic/commit/47a96a25a50d925a7a9368e8050d2f641527ba32))
* **main:** Added test.js with basics ([dbd0eed6](https://github.com/fvdm/nodejs-bitonic/commit/dbd0eed65fd0ec301715aa05663a9ff3609e0b48))

